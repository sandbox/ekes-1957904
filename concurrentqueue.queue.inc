<?php

/**
 * @file
 *   Concurrent queue class.
 */

/**
 * Default extension of SystemQueue.
 */
class ConcurrentQueue extends AdvancedQueue {
  /**
   * Set default concurrency limit.
   *
   * @param int $limit
   *   The limit. Use -1 for no limit.
   */
  public static function setDefaultLimit($limit) {
    variable_set('concurrentqueue_limit_default', $limit);
  }

  /**
   * Get default concurrency limit.
   *
   * @return int
   *   Default limit.
   */
  public static function getDefaultLimit() {
    return variable_get('concurrentqueue_limit_default', -1);
  }

  /**
   * Set lists concurrency limit.
   *
   * @param int $limit
   *   The limit. Use -1 for no limit.
   */
  public function setLimit($limit) {
    variable_set('concurrentqueue_limit_' . $this->name, $limit);
  }

  /**
   * Get list's concurrency limit.
   */
  public function getLimit() {
    return variable_get('concurrentqueue_limit_' . $this->name, ConcurrentQueue::getDefaultLimit());
  }

  public function claimItem($lease_time = 30) {
    // Check concurrency limit.
    if ($this->getLimit() >= 0) {
      $count = $this->numberOfClaimedItems();
      if ($count >= $this->getLimit()) {
        return FALSE;
      }
    }
    return parent::claimItem($lease_time);
  }

  /**
   * Retrieve the number of items claimed.
   *
   * @return int
   *   An integer estimate of the number of items in the queues.
   */
  public function numberOfClaimedItems() {
    return db_query('SELECT count(item_id) FROM {advancedqueue} WHERE name = :name AND expire > 0 AND status = ' . ADVANCEDQUEUE_STATUS_PROCESSING, array(':name' => $this->name))->fetchField();
  }

  public function deleteQueue() {
    variable_del('concurrentqueue_limit_' . $this->name);
    parent::deleteQueue();
  }
}
